const reservas = [
    {
      tipoHabitacion: "standard",
      pax: 1,
      noches: 3
    },
    {
      tipoHabitacion: "standard",
      pax: 1,
      noches: 4
    },
    {
      tipoHabitacion: "suite",
      pax: 2,
      noches: 1
    }
];

class Habitacion {
    constructor(reservas) {
        this._reservas = reservas;
        this._total = 0;
        this._subtotal = 0;
        this.IVA = 0.21;
        this.precioStandard = 100;
        this.precioSuite = 150;
        this.precioAdicional = 40;
    }

    calcTotal() {
        // Calculamos subtotal
        for (let i = 0; i < this._reservas.length; i++) {
            let tipo = this.calcPrecioHabitacion(this._reservas[i]['tipoHabitacion']);
            let pax = this.calcPersonaAdicional(this._reservas[i]['pax']);
            let noches = this._reservas[i]['noches'];
            this._subtotal += ((tipo * noches)) + pax;
        }

        // Calculamos total
        this._total = this._subtotal + (this._subtotal * this.IVA);

        console.log("Precio sin IVA: " + this._subtotal.toFixed(2) +"€");
        console.log("Precio con IVA: " + this._total.toFixed(2) +"€");
    }



    calcPrecioHabitacion(tipoHabitacion) {
        switch (tipoHabitacion) {
            case 'standard':
                return this.precioStandard;
                break;
            case 'suite':
                return this.precioSuite;
                break;
            default:
                break;
          }
    }     

    calcPersonaAdicional(personas) {
        return (personas > 1 ? --personas * this.precioAdicional : 0);
    }
}


const totalHabitacion = new Habitacion(reservas);
totalHabitacion.calcTotal();
  